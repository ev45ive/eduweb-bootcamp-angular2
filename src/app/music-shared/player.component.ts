import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PlayerService } from './player.service'

@Component({
  selector: 'player',
  template: `
    <audio #audio_id controls 
      (play)="playing()" 
      (pause)="paused()" style="width:100%"></audio>
  `,
  styles: []
})
export class PlayerComponent implements OnInit {

  constructor(private player:PlayerService) {
    this.player.getTrackStream()
               .subscribe( track => {
                 if(track)
                  this.play(track)
               })
  }

  playing(){
    this.player.playing()
  }

  paused(){
    this.player.paused()
  }

  @ViewChild('audio_id')
  audioRef:ElementRef;

  play(track){
    let audio = this.audioRef.nativeElement
    audio.volume = 0.1;
    
    if(audio.src != track.preview_url){
      audio.src = track.preview_url;
      audio.play()
    }else if(audio.paused){
      audio.play();
    }else{
      audio.pause();
    }
  }


  ngOnInit() {
  }

}
