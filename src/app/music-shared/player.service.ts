import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs'
//import 'operators/add/map' from 'rxjs'

@Injectable()
export class PlayerService {

  private trackStream = new Subject();
  private playingStream = new Subject();

  constructor() { }

  playTrack(track){
    this.track = track;
    this.trackStream.next(track)
  }

  is_playing = false;

  track;

  getPlayingStream(){
    return this.playingStream.startWith(this.is_playing)
  }

  getTrackStream(){
    return this.trackStream.asObservable()
  }

  playing(){
    this.is_playing = true
    this.playingStream.next(true)
  }

  paused(){
    this.is_playing = false
    this.playingStream.next(false)
  }

}
