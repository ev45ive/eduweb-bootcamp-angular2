import { Component, OnInit, Input } from '@angular/core';
import { PlaylistSelectionService} from './playlist-selection.service'
import { PlayerService } from './player.service'

@Component({
  selector: 'track-list',
  template: `
    <table class="table table-striped">
      <thead>
        <tr>
          <th> # </th>
          <th> Nazwa </th>
          <th> Wykonawca </th>
        </tr>
      </thead>
      <tbody>
        <tr *ngFor="let track of tracks">
          <td> {{track.track_number}} </td>
          <td> {{track.name}} </td>
          <td> {{track.artists[0].name}} </td>
          <td  (click)="play(track)"> 
            <i class="fa" [ngClass]="{
              'fa-play': current !== track,
              'fa-pause': current == track
            }"></i>
          </td>
          <td  (click)="addToPlaylist(track)">Dodaj</td>
        </tr>
      </tbody>
    </table>
  `,
  styles: []
})
export class TrackListComponent implements OnInit {

  @Input()
  tracks

  current;

  play(track){
    this.player.playTrack(track)
  }

  constructor(private selectionService: PlaylistSelectionService,
              private player:PlayerService) { 

                this.current = this.player.track;
                this.player.getTrackStream()
                .subscribe(track=>{
                  this.current = track;
                })

                this.player.getPlayingStream()
                  .filter( playing => playing == false)
                  .subscribe(() => {
                    this.current = null;
                  })

  }

  addToPlaylist(track){
    this.selectionService.addToPlaylist(track)
  }

  ngOnInit() {
  }

}
